
let firstName = "Ian";
let lastName = "Tablada";
let age = 27;

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];

let workAddress = {
	houseNumber: "32", 
	street: "Washington", 
	city: "Lincoln", 
	state: "Nebraska"};


function printUserInfo (){
	console.log(`First Name: ${firstName}`);
	console.log(`Last Name: ${lastName}`);
	console.log(`Age: ${age}`);

	console.log("Hobbies:");
	console.log(hobbies);

	console.log(workAddress);

	console.log(firstName + "  " + lastName + " is "  + age + " years of age.");

	console.log("This was printed inside of the function");


}

printUserInfo();

	
function returnFunction (x){
	
	 x = ["Biking", "Mountain Climbing", "Swimming"];

	return x;
	
}


let y = returnFunction();
console.log(y);

console.log("This was printed inside of the function");



